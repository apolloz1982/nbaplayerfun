//
//  ContentView.swift
//  NBAPlayerFun
//
//  Created by วิโรจน์ ขุนทอง on 9/8/2564 BE.
//

import SwiftUI

struct PlayerDetail: View {
    
    var player: Player
    
    var body: some View {
        VStack {
            Image(player.team.imageName).resizable().aspectRatio(contentMode: .fit)
            Image(player.imageName).clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/).background(Circle().foregroundColor(.white)).overlay(Circle().stroke(Color.white, lineWidth: 4)).offset(x: 0, y: -90).padding(.bottom, -70).shadow(radius: 15)
            
            Text(player.name).font(.system(size: 50)).fontWeight(.heavy).lineLimit(1).padding(.leading).padding(.trailing).minimumScaleFactor(0.5)
           
            StatText(statName: "Age", statValue: "\(player.age)")
            StatText(statName: "Height", statValue: player.height)
            StatText(statName: "Weight", statValue: "\(player.weight)lbs")
            Spacer()
        }.edgesIgnoringSafeArea(.top)
       
    }
}

struct PlayerDetail_Previews: PreviewProvider {
    static var previews: some View {
//       PlayerDetail(player: players[0])
        Group {
            PlayerDetail(player: players[0]).environment(\.sizeCategory, .extraExtraExtraLarge).previewDevice("iPhone 11")
            PlayerDetail(player: players[0]).environment(\.sizeCategory, .extraSmall).previewDevice("iPhone 11")
        }
    }
}
