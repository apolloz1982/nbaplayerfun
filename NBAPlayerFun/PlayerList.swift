//
//  PlayerList.swift
//  NBAPlayerFun
//
//  Created by วิโรจน์ ขุนทอง on 10/8/2564 BE.
//

import SwiftUI

struct PlayerList: View {
    var body: some View {
        NavigationView{
            List(players) {
                currentPlayer in
                NavigationLink(destination: PlayerDetail(player: currentPlayer)){
                    PlayerRow(player: currentPlayer).frame(height: 60)
                }
            }.navigationTitle(Text("NBA Finals Players"))
        }
    }
}

struct PlayerList_Previews: PreviewProvider {
    static var previews: some View {
        PlayerList()
    }
}
