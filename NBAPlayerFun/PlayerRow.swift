//
//  PlayerRow.swift
//  NBAPlayerFun
//
//  Created by วิโรจน์ ขุนทอง on 9/8/2564 BE.
//

import SwiftUI

struct PlayerRow: View {
    
    var player: Player
    
    var body: some View {
        HStack {
            Image(player.imageName).resizable().aspectRatio(contentMode: .fit)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .background(Circle())
                .foregroundColor(player.team.color)
            Text(player.name).font(.largeTitle).lineLimit(1).minimumScaleFactor(0.5)
            Spacer()
        }
        
    }
}

struct PlayerRow_Previews: PreviewProvider {
    static var previews: some View {
        PlayerRow(player: players[0]).previewLayout(.fixed(width: 500, height: 100))
//        PlayerRow(player: players[0]).previewLayout(.device)
//        PlayerRow(player: players[0]).previewDevice("iPhone 11")
//        Group {
//            PlayerRow(player: players[0]).previewDevice("iPhone 11")
//            PlayerRow(player: players[0]).previewDevice("iPhone 8 Plus")
//            PlayerRow(player: players[0]).previewDevice("iPhone 11 Pro Max")
//        }
        
    }
}
