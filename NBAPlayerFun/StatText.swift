//
//  StatText.swift
//  NBAPlayerFun
//
//  Created by วิโรจน์ ขุนทอง on 9/8/2564 BE.
//

import SwiftUI

struct StatText: View {
    
    var statName: String
    var statValue: String
    
    var body: some View {
        HStack(alignment: .center) {
            Text(statName + ":").font(.system(size: 45)).fontWeight(.heavy).padding(.leading, 30)
            Text(statValue).font(.system(size:
                45)).fontWeight(.light).padding(.trailing, 30)
            Spacer()
        }.minimumScaleFactor(0.6)
    }
}

struct StatText_Previews: PreviewProvider {
    static var previews: some View {
        StatText(statName: "Age", statValue: "32")
    }
}
